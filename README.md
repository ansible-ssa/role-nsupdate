# Ansible Role: nsupdate

This role can be used to manage DNS records by using the `nsupdate` command. It expects the DNS Server to be setup by the `playbook-dyndns-server`.

## How to install this role

The Ansible SSA collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

The role is optimized to be used from the [Playbook RHAAP](https://gitlab.com/ansible-ssa/playbook-rhaap) which will be called by a Job Template created by this role.

If you want to use it in your playbook:

```yaml
- name: Update DNS record
  ansible.builtin.include_role:
    name: nsupdate
  vars:
    ipaddress: "{{ address.address }}"
    shortname: "{{ instance_name }}"
    dns_suffix: "{{ dns_suffix }}"
    mode: update
  when: dns_update | bool
```

To remove a DNS record, set the `mode` to **remove**.

```yaml
- name: Remove DNS record
  ansible.builtin.include_role:
    name: nsupdate
  vars:
    shortname: "{{ instance_name }}"
    dns_suffix: "{{ dns_suffix }}"
    mode: remove
  when:
    - dns_update | bool
```

Instead of using the role directly, consider using the [Ansible SSA Collection](https://gitlab.com/ansible-ssa/ansible-ssa-collection)

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
